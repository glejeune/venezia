const babelJest = require('babel-jest').default;
const babelOptions = {
  presets: [
    [
      '@babel/preset-env',
      {
        targets: {
          node: 'current',
        },
      },
    ],
  ],
  babelrc: false,
};
module.exports = babelJest.createTransformer(babelOptions);
