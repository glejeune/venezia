[![npm version](https://badge.fury.io/js/venezia.svg)](https://badge.fury.io/js/venezia)
[![Gitlab CI](https://gitlab.com/glejeune/venezia/badges/main/pipeline.svg)](https://gitlab.com/glejeune/venezia/-/pipelines)
![NPM](https://img.shields.io/npm/l/venezia)

# Venezia

A simple framework to easily create Web Component

_"Why Venezia ?<br />- No special reason, I like this city... So why not ?_

## Get started

First, add [venezia](https://www.npmjs.com/package/venezia) to your project :

```sh
npm install venezia
```

Then, create and register your component :

```js
import Venezia from 'venezia';

class Test extends Venezia {
  render() {
    return `<span>Hello World</span>`;
  }
}

Test.component('my-test');
```

Finaly, use this component in your HTML page :

```html
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>Venezia sample</title>
    <script type="module" src="my-test.js"></script>
  </head>
  <body>
    <my-test></my-test>
  </body>
</html>
```

## Documentation

### Lifecycle

`constructor()` : This is the contructor of your component class. If you add a constructor to your component, you _must_ call `super()` !

`willConnect()` : This function is called when the element is connected to the DOM for the first time, before rendering.

`rendered()` : This function is called when the element has been rendered. Since the `render` function can be async, this function can be called after the `connected` function.

`connected()` : This function is called when the element is connected to the DOM for the first time, juste aften the `render` function has been called.

`<property>Changed(oldValue, newValue)` : This function is called when the `property` change. See [Properties](#properties).

`adopted()` : This function is called when the element is moved to a new document.

`disconnected()` : This function is called when the element is disconnected from the DOM.

### `render` (and `reRender`)

The `render` function must return the HTML code of your component.

This function can be sync:

```js
class Sample extends Venezia {
  render() {
    return `<span>Hello Sync World!</span>`;
  }
}
```

Async:

```js
class Sample extends Venezia {
  async render() {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(`<span>Hello Async World!</span>`);
      }, 1000);
    });
  }
}
```

An async generator:

```js
class Sample extends Venezia {
  async *render() {
    yield `<span>Changing...</span>`;

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(`<span>Hello Async Generated World!</span>`);
      }, 1000);
    });
  }
}
```

You can manually update a component by calling the `reRender()` function.

### `stylesheet`

If you want to add CSS to your component, you can use the the `stylesheet` function. It must return the CSS code for your component:

```js
class Sample extends Venezia {
  stylesheet() {
    return `
    span {
      background-color: red;
      color: #fff;
    }
    `;
  }
}
```

### Properties

All properties added to a component are stored in the `props` attributes. For example, if you declare:

```html
<my-component status="example"></my-component>
```

You can access to the value of the `status` attribute with `this.props['status']` (or `this.props.status`).

If you want to be informed when an attribute is added, removed or updated, you can create a `<property>Changed` function. This function will receive the old and the new value for the attribute. So if you wan't to be informed if the `status` attribut change (in the previous example), you can do this:

```js
class Sample extends Venezia {
  statusChanhed(oldValue, newValue) {
    // Do something with oldValue and newValue...
  }
}
```

> :warning: The `<property>Changed()` functions are only called when the attribute change. But not if you modify the corresponding property (`this.props.status` in our example). However, be aware that when the `<attribute>Changed` function is called; the corresponding property (`this.props.status` in our example) has already been modified by `Venezia`.

### Events

If you want to capture an interaction event in your component, no need to create the corresponding event listener. You just need to create a function for the corresponding event. For example, to capture a click event, create a `click()` function:

```js
class Sample extends Venezia {
  click(event) {
    console.log('click', event);
  }
}
```

### Register your component

To register your component, simply call the static function `component` by passing it the name for your component.

```js
class Sample extends Venezia {
  // ...
}

Sample.component('my-sample');
```

> :warning: Web components require a name with two or more parts.

## Licence

Copyright (c) 2021 Grégoire Lejeune<br />
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer.

2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution.

3. Neither the name of the copyright holder nor the names of its
   contributors may be used to endorse or promote products derived from
   this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
