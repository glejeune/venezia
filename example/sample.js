import Venezia from '../dist/module';

class Test extends Venezia {
  stylesheet() {
    return `
    span {
      background-color: red;
      color: #fff;
    }
    `;
  }

  async *render() {
    yield `<span>Loading...</span>`;

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve(`<span>Hello World</span>`);
      }, 1000);
    });
  }

  willConnect() {
    console.log('willConnect...');
    //...
  }

  connected() {
    console.log('connected...');
    //...
  }

  disconnected() {
    console.log('disconnected...');
    //...
  }

  rendered() {
    console.log('rendered...');
    //...
  }

  reRendered() {
    console.log('reRendered...');
    //...
  }

  // Receive a click event
  click(event) {
    this.reRender();
    console.log('click', event);
    //...
  }

  dataChanged(oldValue, newValue) {
    console.log('data changed from', oldValue, 'to', newValue);
  }

  oldValueChanged(oldValue, newValue) {
    console.log('old-value changed from', oldValue, 'to', newValue);
  }
}

Test.component('my-test');
