module.exports = {
  clearMocks: true,
  collectCoverage: true,
  coverageDirectory: '<rootDir>/coverage',
  moduleDirectories: ['src', 'node_modules'],
  setupFiles: [],
  collectCoverageFrom: ['src/**/*.{js,jsx,ts,tsx}', '!**/node_modules/**'],
  testPathIgnorePatterns: ['/node_modules/', '/lib/'],
  testRegex: '(/src/__tests__/.*\\.(test|spec))\\.(ts|tsx|js)$',
  transform: {
    '^.+\\.jsx?$': '<rootDir>/jest-transformer.js',
  },
};
