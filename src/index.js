export default class extends window.HTMLElement {
  constructor() {
    super();

    this.props = {};

    if (this.attachShadow) {
      this._root = this.attachShadow({ mode: 'open' });
    } else {
      this._root = this;
    }

    this._initialized = false;
    this._hasBeenRendered = false;

    this._events();
  }

  reRender() {
    if (this._hasBeenRendered) {
      this._render();
    }
  }

  connectedCallback() {
    if (!this._initialized) {
      for (const { name, value } of this.attributes) {
        this.props[name] = value;
      }

      this.willConnect && this.willConnect();
      this._render();
      this.connected && this.connected();

      this._initialized = true;
    }
  }

  _renderIterator(style, iterator) {
    return iterator.next().then((result) => {
      this._root.innerHTML = style + result.value;
      if (result.done) return;
      return this._renderIterator(style, iterator);
    });
  }

  _finalizeRender() {
    if (this._hasBeenRendered) {
      this.reRendered && this.reRendered();
    } else {
      this._hasBeenRendered = true;
      this.rendered && this.rendered();
    }
  }

  _render() {
    const style = this.stylesheet ? `<style>${this.stylesheet()}</style>` : '';

    if (this.render instanceof async function () {}.constructor) {
      this.render()
        .then((content) => (this._root.innerHTML = style + content))
        .then(this._finalizeRender.bind(this));
    } else if (this.render instanceof async function* () {}.constructor) {
      this._renderIterator(style, this.render()).then(this._finalizeRender.bind(this));
    } else {
      this._root.innerHTML = style + this.render();
      this._finalizeRender();
    }
  }

  disconnectedCallback() {
    this.disconnected && this.disconnected();
  }

  adoptedCallback() {
    this.adopted && this.adopted();
  }

  _events() {
    const hp = Object.getOwnPropertyNames(window.HTMLElement.prototype);
    const lp = Object.getOwnPropertyNames(Object.getPrototypeOf(this));

    for (const p of lp) {
      if (hp.indexOf('on' + p) === -1) continue;
      this.addEventListener(p, this[p].bind(this));
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (oldValue === newValue || !this._initialized) {
      return;
    }

    this.props[name] = newValue;

    const lp = Object.getOwnPropertyNames(Object.getPrototypeOf(this));
    const changedFun =
      name
        .split('-')
        .map((value, index) => (index === 0 ? value : value.charAt(0).toUpperCase() + value.slice(1)))
        .join('') + 'Changed';
    this[changedFun](oldValue, newValue);
  }

  static get observedAttributes() {
    const lp = Object.getOwnPropertyNames(this.prototype);
    const attributes = [];

    for (const p of lp) {
      const attr = p.match(/^(.*)Changed$/);
      if (attr) {
        const namedAttr = attr[1]
          .split(/(?=[A-Z])/)
          .join('-')
          .toLowerCase();
        attributes.push(namedAttr);
      }
    }

    return attributes;
  }

  static component(name) {
    if (!window.customElements.get(name)) {
      window.customElements.define(name, this);
    }
  }
}
