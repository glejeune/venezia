/**
 * @jest-environment jsdom
 */

import Venezia from '..';

describe('Base', () => {
  afterEach(() => {
    while (document.body.firstChild) {
      document.body.removeChild(document.body.firstChild);
    }
  });

  it('should create a component', () => {
    class TestOne extends Venezia {
      render() {
        return '<div>Test One</div>';
      }
    }
    TestOne.component('test-one');
    const element = document.createElement('test-one');
    document.body.appendChild(element);

    const component = document.querySelector('test-one');
    const div = component.shadowRoot.querySelector('div');
    expect(div.innerHTML).toContain('Test One');
  });

  it('should create a component with props', () => {
    class TestTwo extends Venezia {
      render() {
        return `<div id=${this.props.id}>Test Two</div>`;
      }
    }

    TestTwo.component('test-two');
    const element = document.createElement('test-two');
    element.setAttribute('id', 'test-props');
    document.body.appendChild(element);

    const component = document.querySelector('test-two');
    expect(component.props).toEqual({ id: 'test-props' });

    const div = component.shadowRoot.querySelector('div');
    expect(div.getAttribute('id')).toEqual('test-props');
    expect(div.innerHTML).toContain('Test Two');
  });
});

describe('Stylesheet', () => {
  it('should create a component with stylesheet', () => {
    class TestStylesheet extends Venezia {
      stylesheet() {
        return 'div { color: #abcdef; }';
      }
      render() {
        return '<div>Test Stylesheet</div>';
      }
    }
    TestStylesheet.component('test-stylesheet');
    const element = document.createElement('test-stylesheet');
    document.body.appendChild(element);
    const component = document.querySelector('test-stylesheet');

    const div = component.shadowRoot.querySelector('div');
    expect(div.innerHTML).toContain('Test Stylesheet');

    const style = component.shadowRoot.querySelector('style');
    expect(style.innerHTML).toContain('div { color: #abcdef; }');
  });
});
